import sklearn
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
gnb = GaussianNB()

data =  load_breast_cancer()
label_names = data['target_names']
labels = data['target']
feature_names = data['feature_names']
feature = data['data']
print(label_names)
print(labels[0])
print(feature_names)
print(feature[0])
train, test, train_labels, test_labels = train_test_split(feature, labels, test_size= 0.4, random_state=42)
model = gnb.fit(train, train_labels)
preds = gnb.predict(test)
print (accuracy_score(test_labels, preds))